

Con la version v2 de registry al parecer es posible poner un fortal de nginx y:
	- las uri que comientzan con /v2/ redirigiralas a regisrty:9000
	- las demás pueden crear un sitio web


# Autenticación con PAM nignx


    # En la ruta a proteger
    location /restringido {
           auth_pam "Introduce tus credenciales";
           auth_pam_service_name "nginx";
       }


	echo "
	 auth       include      common-auth
	 account    include      common-account
	" > /etc/pam.d/nginx 

 usermod -aG shadow www-data 
 chown root:shadow /etc/shadow
 chmod g+r /etc/shadow

 systemctl restart nginx 


# Autenticación condicional al método


This is simple as:

 location /v2/{
   client_max_body_size    19000M;

   limit_except GET {
           auth_pam "login";
           auth_pam_service_name "nginx";

   }

   proxy_pass                   http://127.0.0.1:5000;
   proxy_set_header             X-Forwarded-For $remote_addr;



 }

