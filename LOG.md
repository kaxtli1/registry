

## Conditional auth 


I trying without success this approach: the url registry.conexo.mx/v2/hola is traslated to registry.conexo.mx/read/hola  if method = GET.
And is trastadet to to http://127.0.0.1:5000/v2/hola if method = PATCH.

But does't work.



	location /v2/ {
	   # Usando SSL max_body_size debe estar dentro de location
	   client_max_body_size    19000M;

	   set $auth off ;

	#   if ($request_method ~* "^(GET|HEAD)$") {
	#   if ($request_method ~* "(GET|HEAD)$") {
	#        set $auth "off";
	#           rewrite ^/v2/(.*)$ /read/$1 last;

	#           rewrite ^(.*)$ /read/$1 break;
	#   }

	   if ($request_method != PATCH) {
	           rewrite ^/v2/(.*)$ /read/$1 last;

	   }

	#   auth_basic                   $auth ;
	#   auth_basic_user_file         /run/secrets/htpasswd;

	#           auth_pam $auth;
	#   auth_pam $auth;

	}


        location /read/ {
	   proxy_pass                   http://127.0.0.1:5000/v2/;
	   proxy_set_header             X-Forwarded-For $remote_addr;


        }


This is the correct code:

