

# Tarea 1. Terminada

Crear un pod y/o un docker-compose para poner el frontal de registry con **autenticación condicional** 
Lectura abierta, escritura solo con autenticación.



 # if is evil, but not with rewrite ( 'set' is part of rewrite module )
 location /v2/ {
   # Usando SSL max_body_size debe estar dentro de location
   client_max_body_size    19000M;

   set $auth "Docker" ;

   # Leer no requiere autorización
   if ($request_method ~* "^(GET|HEAD)$") {
        set $auth off;
   }

   # podman login consulta GET /v2/ para authenticar 
   if ($uri = "/v2/" ) {
        set $auth "Solo";
   }

   auth_basic                    $auth ;
   auth_basic_user_file          /etc/nginx/registry_users;

   proxy_pass              	http://localhost:5000;
   proxy_set_header 		X-Forwarded-For $remote_addr;

 }


# Tarea 2:podman play InitContainer

corregir el archivo /etc/nginx/conf.d/registry.conf

	- elimmaar app.conf
	- eliminar default


