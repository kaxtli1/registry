#!/bin/bash
# 2022-02

# TODO: Verificar el paso de argumentos

PATH=/usr/local/bin/:$PATH

# Hay dos lugares para poner el certificado:
#	/etc/ssl/		Estandar para todos los contenedores.
#	/etc/docker/ssl		Especial para esta imagen. ( un solo directorio de configuración )


#openssl req  -newkey rsa:4096 -nodes -sha256 \
#	-keyout /etc/ssl/private/server.key -x509 -days 365 \
#	-addext "subjectAltName = DNS:localhost" \
#	-out /etc/ssl/certs/server.crt

openssl req -new -x509 -days 3650 -nodes \
	-out /etc/ssl/certs/server.crt \
	-keyout /etc/ssl/private/server.key \
	-subj "/C=MX/ST=Estado/L=Lugar/O=$SSL_DOMAIN/CN=$SSL_ORGANIZATION"


# Activa la sección de certifitado autofirmado en la configuración de docker-registry
sed 's/^#1#//' -i /etc/docker/registry/config.yml

###### Autenticación básica

htpasswd -b -Bc /etc/docker/registry/htpasswd $REGISTRY_USER $REGISTRY_PASSWORD

# Activa la sección de autenticación en la configuración de docker-registry
sed 's/^#3#//' -i /etc/docker/registry/config.yml


###### Certificado de letsencript

#TAREA: debe exponerse el puerto 80 y 443 temporalmente
#TAREA: debe definirse un mecanismo de actualización 
#letsencrypt certonly --standalone -n -m $SSL_MAIL --agree-tos  -d $SSL_DOMAIN
#sed 's/^#2#//' -i /etc/docker/registry/config.yml

