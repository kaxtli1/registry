# 2021
# 2022-07-27


# Este tipo de certificados está obsoleto, usar certificados SAN
dominio=$1
organizacion=$2

echo =={ $C $ST $L $O $CN }==


if [ "$2" == "" ] ; then
  echo "USO: $0 dominio.ltd  organizacion "
  return 1
fi


openssl req -new -x509 -days 3650 -nodes \
	-out /etc/ssl/certs/server.crt \
	-keyout /etc/ssl/private/server.key \
	-subj "/C=MX/ST=Estado/L=Lugar/O=$organizacion/CN=$dominio"

# Imprime las huellas
openssl x509 -in /etc/ssl/certs/server.crt -fingerprint -md5    -noout
openssl x509 -in /etc/ssl/certs/server.crt -fingerprint -sha256 -noout

