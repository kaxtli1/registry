# 2022-03-03
# Registro con dos contenedores
#    - registry
#    - nginx, autenticación condicional ( usuario para escritura, abierto para lectura )
# revisar la implementacion en nginx_conf:/etc/nginx/conf.d/

# TAREA: Crear el docker-file


podman pod rm -f registry.pod

podman volume rm nginx_conf
podman secret rm htpasswd

# Crea el pod
podman pod create --name registry.pod -p 80:80


# Agrega el rigistro
podman run --pod registry.pod -d --name registry.registry.pod  \
  -v nginx_conf:/etc/nginx/conf.d/ \
  registry.conexo.mx/kaxtli/registry


# Agrega contraseña
podman secret create htpasswd htpasswd

# Agrega niginx con autenticación base
podman run --pod registry.pod -d --name nginx.registry.pod \
  --volumes-from registry.registry.pod \
  --secret source=htpasswd,type=mount,uid=33,gid=33,mode=777\
   registry.conexo.mx/kaxtli/nginx
