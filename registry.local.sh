# 2023-02-12 Quiron
# Levanta registry.conexo.mx

# El problema es que al usar registry.yaml podman play kebe intenta hacer conexión con el registro antes de buscar las imagenes locales.
# Es decir. El registro debe responder, sino podman play no puede instanciar el pod.
# este guión levanta un registro privado local para poder descargar las imagenes y luego crear el registro público
# Esto es en la versión 3.0 de podman.



# Instancia un registro de acceso local sin https
podman run -d --name registry.local kaxtli/registry

# Agrega la IP a /etc/hosts
echo $(podman inspect registry.local  | grep IPAddress | head -1 | cut -d\" -f4 ) registry.local >> /etc/hosts

# Sube las imagenes necesarios para registry.yaml
podman push --tls-verify=false registry.local:5000/kaxtli/registry:latest
podman push --tls-verify=false registry.local:5000/kaxtli/letsencrypt:latest
podman push --tls-verify=false registry.local:5000/kaxtli/nginx:latest
podman push --tls-verify=false registry.local:5000/kaxtli/nginx-pam:latest


# No es necesario
cat <<EOF >> /etc/containers/registries.conf
    [[registry]]
    location = "registry.local"
    insecure = true

EOF

# Levanta registry.conexo.mx
# Podman 3.0 no puede interpretar initContainers, asi que se creo registry-ini.yaml

podman play kube --tls-verify=false registry-ini.yaml
podman rm -f registry.pod

# Posiblemente sea necesario borrar el volumen nginx_conf y repetir el proceso.
# Esto porque la instanciación es muy rápida, no espera a que un initContainer termine

podman pod rm -f registry.pod
podman volume rm nginx_conf
podman play  kube --tls-verify=false registry-ini.yaml

# Levanta el registro ya inicializado
podman play kube --tls-verify=false registry.yaml
