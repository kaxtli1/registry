# Oscar P 

	2021-1013
	2022-0227

	Volumenes
	/etc/docker/registry
	/var/lib/docker-registry
	/etc/ssl


# Introducción


Esta imagen de registry tiene diferentes formas de utilizarse. 

1. Sin usuarios y sin certificado.
2. Con certificado autofirmado
3. 

Estas  imagenes nerecitan dos momentos para ejecutar.

	- Instanciar
	- Ejecutar


La instanciación se realiza con el guión /etc/local/bin/install.sh ( no se utiliza instantiate.sh )


	#    Warning: It’s not possible to use an insecure registry with basic authentication.
	# https://www.redhat.com/sysadmin/manage-container-registries

# Instanciar un contenedor sin certificado

La forma más simple de uso de esta imagen, es crear un contenedor con un registro sin seguridad. No hay usuarios, ni certificados SSL. Cualquiera puede subir y descargar imagenes.


	podman build -t kaxtli/registry .
	podman run -d --name registry -p 80:5000  kaxtli/registry
	podman push --tls-verify=false localhost/kaxtli/registry:latest
	


# Instanciar un contenedor con certificado auto-firmado



	podban build -t kaxtli/registry .


Instalar - Instanciar


	podman run --rm --entrypoint install.sh \
	-v registry_ssl:/etc/ssl \
	-v registry_conf:/etc/docker \
	-e SSL_DOMAIN=localhost \
	-e SSL_ORGANIZATION=Conacyt  kaxtli/registry

Ejecutar


	podman run -d --name registry \
	--replace \
	-p 443:5000 
	-v ssl:/etc/ssl \
	-v conf:/etc/docker kaxtli/registry

 si letsencrypt

	podman run -d --name registry --replace -p 443:5000 -v v /etc/letsencrypt/:/etc/letsencrypt/ -v conf:/etc/docker kaxtli/registry



## Subir o bajar

Configurar el registro del cliente. Se declara el registro como inseguro para aceptar el certificado autofirmado.


	cat <<EOF >> /etc/containers/registries.conf
	[[registry]]
	location = "localhost"
	insecure = true

	EOF



Es necesesario agregar la variable de entorno GODEBUG para aceptar certificados obsoletos.


	export GODEBUG="x509ignoreCN=0"

	podman push localhost/kaxtli/registry:latest
	




# nginx

	bash pod.sh

Creará un pod con nginx como frontal para realizar la autenticación condicional:
- Lectura.  Todo mundo
- Escritura. Acceso con contraseá. ( simple_auth )


# Borrar imagenes del registro

Consultar primero el digest de la imagen
    registry=registry.conexo.mx
    image=kaxtli/sagitta-mono
    tag=alpha
    user=debian
    #passwd=124

    digest=$(curl -sSL -I  -u "${user}:${passwd}" -H "Accept: application/vnd.oci.image.manifest.v1+json"  https://${registry}/v2/${image}/manifests/${tag} | grep Docker-Content-Digest: | sed 's/.*Digest: //' )
    curl  -u "${user}:${passwd}"  -s -X DELETE https://${registry}/v2/${image}/manifests/${digest}


No se en que ocasiones se usaba:
    application/vnd.docker.distribution.manifest.v2+json
    application/vnd.oci.image.manifest.v1+json


Borrar todas

    imagenes=$(podman search registry.conexo.mx/kaxtli/ --format "table {{.Name}}" | cut -d/ -f3 )
    
La forma más simple de borrar todos:

    rm -fr /var/lib/docker-registry/docker/registry/v2/blobs/sha256/*
    rm -fr /var/lib/docker-registry/docker/registry/v2/repositories/kaxtli/



